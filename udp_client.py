import socket
import base64
import struct
from Crypto.Cipher import AES

def stringToBase64(s):
    return base64.b64encode(s.encode('utf-8'))

def base64ToString(b):
    return base64.b64decode(b).decode('utf-8')
    
def convert_string_to_bytes(string):
    bytes = b''
    for i in string:
        bytes += struct.pack("B", ord(i))
    return bytes 

def calc_base64_length(str_len):
	return int((4 * str_len / 3) + 3) & ~3
	      
def cmd_to_sixteen(string):
	while True:
		if calc_base64_length(len(string))%16==0:
			return string
		else:
			string+=" "
def base_to_sixteen(base):
	while True:
		if len(base)%16==0:
			return base
		else:
			base+=b'='


    
key = 'ANAL\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'#'\x15\x2B\x7E\x16\x28\xAE\xD2\xA6\xAB\xF7\x15\x88\x09\xCF\x4F\x3C'
vi =  '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
key = convert_string_to_bytes(key)
vi = convert_string_to_bytes(vi)
	
command = "CONF:LAMP:15:10:10:10:1:000:000:000"
serverAddressPort   = ('192.168.85.255', 8888)
bufferSize          = 1024
# Create a UDP socket at client side
UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDPClientSocket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
# Send to server using created UDP socket


def encrypt(cmd):
	global key
	global vi
	cipher = AES.new(key, AES.MODE_CBC, vi)
	#cmd = cmd_to_sixteen(cmd)
	cmd = base64.b64encode(cmd.encode())
	cmd = cipher.encrypt(base_to_sixteen(cmd))
	cmd = base64.b64encode(cmd)
	return cmd
	
def decrypt(resp):
	global key
	global vi
	cipher = AES.new(key, AES.MODE_CBC, vi)
	resp = base64.b64decode(resp.decode('utf-8'))
	resp = cipher.decrypt(resp)
	print(len(resp))
	resp = base64.b64decode(resp)
	return resp.decode('utf-8')


def lampa_test():
    UDPClientSocket.sendto(encrypt(command), serverAddressPort)
    msgFromServer = UDPClientSocket.recvfrom(bufferSize)
    response = decrypt(msgFromServer[0])
    print("Response: " + str(response))


def monitor():
	UDPClientSocket.bind(serverAddressPort)
	msgFromServer = UDPClientSocket.recvfrom(bufferSize)
	print(msgFromServer[0])

lampa_test()
    

