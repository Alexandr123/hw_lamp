/*
  Скетч к проекту "Многофункциональный RGB светильник"
  Страница проекта (схемы, описания): https://alexgyver.ru/GyverLamp/
  Исходники на GitHub: https://github.com/AlexGyver/GyverLamp/
  Нравится, как написан код? Поддержи автора! https://alexgyver.ru/support_alex/
  Автор: AlexGyver, AlexGyver Technologies, 2019
  https://AlexGyver.ru/
*/
#include <FS.h>
/*
  Версия 1.5:
  - Исправлено непереключение кнопкой с первого на последний режимы
  - Добавлена настройка для отключения кнопки (для корректной работы схемы без кнопки)
  - Убран статический IP для локального режима (вызывал проблемы)
  - Добавлена возможность сброса настроек WiFi удержанием кнопки при включении лампы (~7 секунд)
  - Добавлен вывод IP адреса на лампу по пятикратному нажатию на кнопку
*/
// Ссылка для менеджера плат:
// http://arduino.esp8266.com/stable/package_esp8266com_index.json

// ============= НАСТРОЙКИ =============
// -------- КНОПКА -------
#define USE_BUTTON 1    // 1 - использовать кнопку, 0 - нет

// -------- ВРЕМЯ -------
#define GMT 2              // смещение (москва 3)
#define NTP_ADDRESS  "europe.pool.ntp.org"    // сервер времени

// -------- РАССВЕТ -------
#define DAWN_BRIGHT 200       // макс. яркость рассвета
#define DAWN_TIMEOUT 1        // сколько рассвет светит после времени будильника, минут

// ---------- МАТРИЦА ---------
#define BRIGHTNESS 255         // стандартная маскимальная яркость (0-255)
#define CURRENT_LIMIT 200000    // лимит по току в миллиамперах, автоматически управляет яркостью (пожалей свой блок питания!) 0 - выключить лимит

#define WIDTH 72              // ширина матрицы
#define HEIGHT 10            // высота матрицы

#define COLOR_ORDER GRB       // порядок цветов на ленте. Если цвет отображается некорректно - меняйте. Начать можно с RGB

#define MATRIX_TYPE 0         // тип матрицы: 0 - зигзаг, 1 - параллельная
#define CONNECTION_ANGLE 0    // угол подключения: 0 - левый нижний, 1 - левый верхний, 2 - правый верхний, 3 - правый нижний
#define STRIP_DIRECTION 0     // направление ленты из угла: 0 - вправо, 1 - вверх, 2 - влево, 3 - вниз
// при неправильной настройке матрицы вы получите предупреждение "Wrong matrix parameters! Set to default"
// шпаргалка по настройке матрицы здесь! https://alexgyver.ru/matrix_guide/

// --------- ESP --------
#define ESP_MODE 1
// 0 - точка доступа
// 1 - локальный
byte IP_AP[] = {192, 168, 4, 66};   // статический IP точки доступа (менять только последнюю цифру)

// ----- AP (точка доступа) -------
#define AP_SSID "GyverLamp"
#define AP_PASS "12345678"
#define AP_PORT 8888

// -------- Менеджер WiFi ---------
#define AC_SSID "AutoConnectAP"
#define AC_PASS ""

// ============= ДЛЯ РАЗРАБОТЧИКОВ =============
#define LED_PIN 2             // пин ленты
#define BTN_PIN 4
#define MODE_AMOUNT 19

#define NUM_LEDS WIDTH * HEIGHT
#define SEGMENTS 1            // диодов в одном "пикселе" (для создания матрицы из кусков ленты)
// ---------------- БИБЛИОТЕКИ -----------------
#define FASTLED_INTERRUPT_RETRY_COUNT 0
#define FASTLED_ALLOW_INTERRUPTS 0
#define FASTLED_ESP8266_RAW_PIN_ORDER
#define NTP_INTERVAL 60 * 1000    // обновление (1 минута)

#include "timerMinim.h"
#include <FastLED.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoJson.h> 
#include <WiFiUdp.h>
#include <EEPROM.h>
#include <NTPClient.h>
#include <GyverButton.h>
#include "fonts.h"

// ------------------- AES --------------------
#include <AES_master.h>
#include <base64_my.h>
AES aes;
// Our AES key. Note that is the same that is used on the Node-Js side but as hex bytes.
byte key[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
// The unitialized Initialization vector
byte my_iv[N_BLOCK] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void aes_reset_vi(){
    for(int i = 0; i < N_BLOCK; i++){
      my_iv[i] = 0;
    }
}

String encrypt(String str) {  
    char b64data[200];
    byte cipher[1000];
    byte iv [N_BLOCK];
    aes_reset_vi();
    aes.set_key( key , sizeof(key));  // Get the globally defined key
    int b64len = base64_encode(b64data, (char *)str.c_str(),str.length());
    // Encrypt! With AES128, our key and IV, CBC and pkcs7 padding    
    aes.do_aes_encrypt((byte *)b64data, b64len , cipher, key, 128, my_iv);
    base64_encode(b64data, (char *)cipher, aes.get_size() );
    Serial.println ("Encrypted data in base64: " + String(b64data) );

    return String(b64data);
}

String decrypt(String str) {
    char b64data[200];
    byte cipher[1000];
    byte iv [N_BLOCK];
    aes_reset_vi();
    aes.set_key( key , sizeof(key));  // Get the globally defined key
    int b64len = base64_decode(b64data, (char *)str.c_str(),str.length());
    aes.do_aes_decrypt((byte *)b64data, b64len , cipher, key, 128, my_iv);
    base64_decode(b64data, (char *)cipher, aes.get_size() );
    Serial.println ("Decrypted data in base64: " + String(b64data) );

    return String(b64data);
}

// ------------------- ТИПЫ --------------------
CRGB leds[NUM_LEDS];
WiFiServer server(80);
WiFiUDP Udp;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, GMT * 3600, NTP_INTERVAL);
timerMinim timeTimer(3000);
GButton touch(BTN_PIN, HIGH_PULL, NORM_OPEN);
// ------------------- WIFI_MANAGER --------------------
#define NAME_LENGTH               30
char wifimngr_netPass[16] = "\0";
char wifimngr_name[NAME_LENGTH] = "\0";
#define NAME_EEM_START            203//max 233
#define NET_PASS_EEM_START        240//NAME_EEM_START+NAME_LENGTH+7
// ----------------- ПЕРЕМЕННЫЕ ------------------
String name = "";
String type = "LAMP";

const char* autoConnectSSID = AC_SSID;
const char* autoConnectPass = AC_PASS;
const char AP_NameChar[] = AP_SSID;
const char WiFiPassword[] = AP_PASS;
unsigned int localPort = AP_PORT;
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1]; //buffer to hold incoming packet
String inputBuffer;
static const byte maxDim = max(WIDTH, HEIGHT);
struct {
  byte brightness = 50;
  byte speed = 30;
  byte scale = 40;
  byte r = 255;
  byte g = 255;
  byte b = 255;
} modes[MODE_AMOUNT];

struct {
  boolean state = false;
  int time = 0;
} alarm[7];

byte dawnOffsets[] = {5, 10, 15, 20, 25, 30, 40, 50, 60};
byte dawnMode;
boolean dawnFlag = false;
long thisTime;
boolean manualOff = false;

int8_t currentMode = 0;
boolean loadingFlag = true;
boolean ONflag = true;
boolean wifeResetSettings = false;
uint32_t eepromTimer;
boolean settChanged = false;
// Конфетти, Огонь, Радуга верт., Радуга гориз., Смена цвета,
// Безумие 3D, Облака 3D, Лава 3D, Плазма 3D, Радуга 3D,
// Павлин 3D, Зебра 3D, Лес 3D, Океан 3D,

unsigned char matrixValue[8][16];
String lampIP = "";

void setup() {
  Serial.begin(115200);
  ESP.wdtDisable();

  // ЛЕНТА
  FastLED.addLeds<WS2812B, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS)/*.setCorrection( TypicalLEDStrip )*/;
  FastLED.setBrightness(BRIGHTNESS);
  if (CURRENT_LIMIT > 0) FastLED.setMaxPowerInVoltsAndMilliamps(5, CURRENT_LIMIT);
  FastLED.show();
  
  touch.setStepTimeout(100);
  touch.setClickTimeout(500);

  Serial.println();

  delay(1000);
  // WI-FI
  if (ESP_MODE == 0) {    // режим точки доступа
    WiFi.softAPConfig(IPAddress(IP_AP[0], IP_AP[1], IP_AP[2], IP_AP[3]),
                      IPAddress(192, 168, 4, 1),
                      IPAddress(255, 255, 255, 0));

    WiFi.softAP(AP_NameChar, WiFiPassword);
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("Access point Mode");
    Serial.print("AP IP address: ");
    Serial.println(myIP);

    server.begin();
  } else {                // подключаемся к роутеру
    Serial.print("WiFi manager");
    WiFiManager wifiManager;
    
    WiFiManagerParameter custom_aes_pass("AES PASS", "Device Password", wifimngr_netPass, 16);
    WiFiManagerParameter custom_aes_name("Name", "Device Name", wifimngr_name, 16);
    wifiManager.addParameter(&custom_aes_pass);
    wifiManager.addParameter(&custom_aes_name);
    wifiManager.setDebugOutput(false);

#if (USE_BUTTON == 1)
    if (!digitalRead(BTN_PIN)){
      wifiManager.resetSettings();
    }
#endif

    wifiManager.autoConnect(autoConnectSSID, autoConnectPass);
    /*WiFi.config(IPAddress(IP_STA[0], IP_STA[1], IP_STA[2], IP_STA[3]),
                IPAddress(192, 168, 1, 1),
                IPAddress(255, 255, 255, 0));*/
    Serial.print("Connected! IP address: ");
    Serial.println(WiFi.localIP());
    lampIP = WiFi.localIP().toString();
    // EEPROM
    EEPROM.begin(512);//202
    strcpy(wifimngr_netPass, custom_aes_pass.getValue());
    strcpy(wifimngr_name, custom_aes_name.getValue());
    Serial.println(custom_aes_pass.getValue());
    Serial.println(custom_aes_name.getValue());

    
    //#define NAME_EEM_START           
    //#define NET_PASS_EEM_START        
    if(wifimngr_name[0]!='\0'){
      name = String(wifimngr_name);
      Serial.println("New name:");
      Serial.println(name);
      for(int i = 0; i < name.length(); i++){
        char ch = name.charAt(i);
        if(ch==':')ch=';';
        EEPROM.write(NAME_EEM_START+i, ch);
      }
      if(name.length()!=NAME_LENGTH){
        EEPROM.write(NAME_EEM_START+name.length(), '\0');
      }
      EEPROM.commit();
    }else{
      char temp[NAME_LENGTH];
      for (uint16_t i = 0; i < NAME_LENGTH; i++){
        char ch = EEPROM.read(NAME_EEM_START+i);
        if(ch!='\0'){
          temp[i]=ch;
        }else{
          name = String(temp);
          Serial.println("From EEPROM name");
          Serial.println(name);
          break;
        }
      }
    }
    
    String tmp = String(wifimngr_netPass);
    if(wifimngr_netPass[0]!='\0'){
      Serial.println("New key:");
      Serial.println(tmp);
      for (uint16_t i = 0; i < 16; i ++){
        if(i < tmp.length()){
          key[i]=tmp.charAt(i);
        }else{
          key[i]='\0';
        }
        EEPROM.write(NET_PASS_EEM_START+i, key[i]);
      }
      EEPROM.commit();
    }else{
      for (uint16_t i = 0; i < 16; i ++){
        char ch = EEPROM.read(NET_PASS_EEM_START+i);
        key[i] = ch;
      }
      Serial.println("From EEPROM key");
      for (uint16_t i = 0; i < 16; i ++){
        Serial.print(char(key[i]));
      }
      Serial.println();
    }
  }
  Serial.printf("UDP server on port %d\n", localPort);
  Udp.begin(localPort);


  delay(50);
  if (EEPROM.read(198) != 20) {   // первый запуск
    EEPROM.write(198, 20);
    EEPROM.commit();

    for (byte i = 0; i < MODE_AMOUNT; i++) {
      EEPROM.put(3 * i + 40, modes[i]);
      EEPROM.commit();
    }
    EEPROM.write(200, 0);   // режим
    EEPROM.commit();
  }
  for (byte i = 0; i < MODE_AMOUNT; i++) {
    EEPROM.get(3 * i + 40, modes[i]);
  }
  currentMode = (int8_t)EEPROM.read(200);

  // отправляем настройки
  sendCurrent();
  char reply[inputBuffer.length() + 1];
  inputBuffer.toCharArray(reply, inputBuffer.length() + 1);
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  Udp.write(reply);
  Udp.endPacket();

  timeClient.begin();
  memset(matrixValue, 0, sizeof(matrixValue));

  randomSeed(micros());
}

void loop() {
  parseUDP();
  effectsTick();
  eepromTick();
  timeTick();
#if (USE_BUTTON == 1)
  buttonTick();
#endif
  ESP.wdtFeed();   // пнуть собаку
  yield();  // ещё раз пнуть собаку

  if(wifeResetSettings){
    WiFiManager wifiManager;
    wifiManager.resetSettings();
    ESP.restart();
  }
}

void eeWriteInt(int pos, int val) {
  byte* p = (byte*) &val;
  EEPROM.write(pos, *p);
  EEPROM.write(pos + 1, *(p + 1));
  EEPROM.write(pos + 2, *(p + 2));
  EEPROM.write(pos + 3, *(p + 3));
  EEPROM.commit();
}

int eeGetInt(int pos) {
  int val;
  byte* p = (byte*) &val;
  *p        = EEPROM.read(pos);
  *(p + 1)  = EEPROM.read(pos + 1);
  *(p + 2)  = EEPROM.read(pos + 2);
  *(p + 3)  = EEPROM.read(pos + 3);
  return val;
}
