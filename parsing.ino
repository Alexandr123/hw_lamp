String make_mul_sixteen(String mul_str){
  for(int i = 0; i < 16; i++){
    if(mul_str.length()%16 == 0){
      return mul_str;
    }else{
      mul_str += " ";
    }
  }
}
String parseDecode(String decode_str){
  String decrypted = decrypt(decode_str);  
  return decrypted;
}
String parseEncode(String encode_str){
  String encrypted = encrypt(encode_str);
  return encrypted;
}
void sendCurrent() {
  //parseDecode(parseEncode("PING"));
  inputBuffer = "GETS";
  inputBuffer += ":";
  inputBuffer += String(name);
  inputBuffer += ":";
  inputBuffer += String(currentMode);
  inputBuffer += ":";
  inputBuffer += String(modes[currentMode].brightness);
  inputBuffer += ":";
  inputBuffer += String(modes[currentMode].speed);
  inputBuffer += ":";
  inputBuffer += String(modes[currentMode].scale);
  inputBuffer += ":";
  inputBuffer += String(ONflag);
  if(currentMode==18){
    inputBuffer += ":";
    inputBuffer += String(modes[currentMode].r);
    inputBuffer += ":";
    inputBuffer += String(modes[currentMode].g);
    inputBuffer += ":";
    inputBuffer += String(modes[currentMode].b);
  }
  inputBuffer = parseEncode(inputBuffer);
}
String getValue(String data, char separator, int index){
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
bool checkName(String _name_){//1
  //return true;
  Serial.println(name);
  Serial.println(_name_);
  if(name==_name_){
    return true;
  }else{
    return false;
  }
}
void pars_setEffect(byte eff, uint8_t red, uint8_t green, uint8_t blue){//2
  saveEEPROM();
  currentMode = eff;
  if(currentMode==18){
    modes[currentMode].r = red;
    modes[currentMode].g = green;
    modes[currentMode].b = blue;
  }
  loadingFlag = true;
  FastLED.clear();
  delay(1);
  sendCurrent();
  FastLED.setBrightness(modes[currentMode].brightness);
}
void pars_setBrightness(uint16_t brgt){//3
  modes[currentMode].brightness = brgt;
  FastLED.setBrightness(modes[currentMode].brightness);
  settChanged = true;
  eepromTimer = millis();
}
void pars_setSpeed(uint16_t spd){//4
  modes[currentMode].speed = spd;
  loadingFlag = true;
  settChanged = true;
  eepromTimer = millis();
}
void pars_setScale(uint16_t sca){//5
  modes[currentMode].scale = sca;
  loadingFlag = true;
  settChanged = true;
  eepromTimer = millis();
}
void pars_setOnOff(uint16_t onoff){
  if(onoff){
    ONflag = true;
    changePower();
    sendCurrent();
  }else{
    ONflag = false;
    changePower();
    sendCurrent();
  }
}

void parsePING(){
  String mac = WiFi.macAddress();
  mac.replace(":", ";");
  String ip = WiFi.localIP().toString();
  if(ONflag){
    inputBuffer = "PING:" + name + ":" + type + ":" + mac + ":" + ip + ":" + "1";// + "*************";//7
  }else{
    inputBuffer = "PING:" + name + ":" + type + ":" + mac + ":" + ip + ":" + "0";// + "*************";
  }
  inputBuffer = parseEncode(inputBuffer);
}
void parseCONF(){
  String _name = getValue(inputBuffer, ':', 1);
  uint16_t _mode = (byte)getValue(inputBuffer, ':', 2).toInt();
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint16_t _brig;
  uint16_t _speed;
  uint16_t _scale;
  uint16_t _onoff;
  _brig = getValue(inputBuffer, ':', 3).toInt();
  _speed = getValue(inputBuffer, ':', 4).toInt();
  _scale = getValue(inputBuffer, ':', 5).toInt();
  _onoff = getValue(inputBuffer, ':', 6).toInt();
  if(_mode==18){
    r = getValue(inputBuffer, ':', 7).toInt();
    g = getValue(inputBuffer, ':', 8).toInt();
    b = getValue(inputBuffer, ':', 9).toInt();
  }
  
  if(checkName(_name)){
    if(_mode==18){
      pars_setEffect(_mode, r, g, b);
    }else{
      pars_setEffect(_mode, 0, 0, 0);
    }
    pars_setBrightness(_brig);
    pars_setSpeed(_speed);
    pars_setScale(_scale);
    pars_setOnOff(_onoff);
    saveEEPROM();
    EEPROM.write(200, currentMode);   // режим
    EEPROM.commit();
    sendCurrent();
  }
}
void parseGETS(){
  String _name = getValue(inputBuffer, ':', 1);
  if(checkName(_name)){
    sendCurrent();
  }
}

void parseUDP() {
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    packetBuffer[n] = 0;
    inputBuffer = packetBuffer;
    inputBuffer = parseDecode(inputBuffer);

    if (inputBuffer.startsWith("PING")) {
      parsePING();
    }
    if (inputBuffer.startsWith("CONF")) {
      parseCONF();
    }
    if (inputBuffer.startsWith("GETS")) {
      parseGETS();
    }

    if (inputBuffer.startsWith("DEB")) {
      inputBuffer = "OK " + timeClient.getFormattedTime();
    } else if (inputBuffer.startsWith("GET")) {
      // sendCurrent();
    } else if (inputBuffer.startsWith("EFF")) {
      // saveEEPROM();
      // currentMode = (byte)inputBuffer.substring(3).toInt();
      // loadingFlag = true;
      // FastLED.clear();
      // delay(1);
      // sendCurrent();
      // FastLED.setBrightness(modes[currentMode].brightness);
    } else if (inputBuffer.startsWith("BRI")) {
      // modes[currentMode].brightness = inputBuffer.substring(3).toInt();
      // FastLED.setBrightness(modes[currentMode].brightness);
      // settChanged = true;
      // eepromTimer = millis();
    } else if (inputBuffer.startsWith("SPD")) {
      // modes[currentMode].speed = inputBuffer.substring(3).toInt();
      // loadingFlag = true;
      // settChanged = true;
      // eepromTimer = millis();
    } else if (inputBuffer.startsWith("SCA")) {
      // modes[currentMode].scale = inputBuffer.substring(3).toInt();
      // loadingFlag = true;
      // settChanged = true;
      // eepromTimer = millis();
    } else if (inputBuffer.startsWith("P_ON")) {
      // ONflag = true;
      // changePower();
      // sendCurrent();
    } else if (inputBuffer.startsWith("P_OFF")) {
      // ONflag = false;
      // changePower();
      // sendCurrent();
    }
    //else if (inputBuffer.startsWith("ALM_SET")) {
    //   byte alarmNum = (char)inputBuffer[7] - '0';
    //   alarmNum -= 1;
    //   if (inputBuffer.indexOf("ON") != -1) {
    //     alarm[alarmNum].state = true;
    //     inputBuffer = "alm #" + String(alarmNum + 1) + " ON";
    //   } else if (inputBuffer.indexOf("OFF") != -1) {
    //     alarm[alarmNum].state = false;
    //     inputBuffer = "alm #" + String(alarmNum + 1) + " OFF";
    //   } else {
    //     int almTime = inputBuffer.substring(8).toInt();
    //     alarm[alarmNum].time = almTime;
    //     byte hour = floor(almTime / 60);
    //     byte minute = almTime - hour * 60;
    //     inputBuffer = "alm #" + String(alarmNum + 1) +
    //                   " " + String(hour) +
    //                   ":" + String(minute);
    //   }
    //   saveAlarm(alarmNum);
    // } else if (inputBuffer.startsWith("ALM_GET")) {
    //   sendAlarms();
    // } else if (inputBuffer.startsWith("DAWN")) {
    //   dawnMode = inputBuffer.substring(4).toInt() - 1;
    //   saveDawnMmode();
    // }
    char reply[inputBuffer.length() + 1];
    inputBuffer.toCharArray(reply, inputBuffer.length() + 1);
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(reply);
    Udp.endPacket();
  }
}
// void sendCurrent() {
//   inputBuffer = "CURR";
//   inputBuffer += " ";
//   inputBuffer += String(currentMode);
//   inputBuffer += " ";
//   inputBuffer += String(modes[currentMode].brightness);
//   inputBuffer += " ";
//   inputBuffer += String(modes[currentMode].speed);
//   inputBuffer += " ";
//   inputBuffer += String(modes[currentMode].scale);
//   inputBuffer += " ";
//   inputBuffer += String(ONflag);
// }

void sendAlarms() {
  inputBuffer = "ALMS ";
  for (byte i = 0; i < 7; i++) {
    inputBuffer += String(alarm[i].state);
    inputBuffer += " ";
  }
  for (byte i = 0; i < 7; i++) {
    inputBuffer += String(alarm[i].time);
    inputBuffer += " ";
  }
  inputBuffer += (dawnMode + 1);
}
